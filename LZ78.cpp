#include <iostream>
#include <vector>
#include <utility>
#include <string>
#include <cctype>
#include <unordered_map>
#include <bitset>
#include <math.h>
#include <algorithm>
#include <iostream>
#include <fstream>

using namespace std;

string value_to_bit(int size, char c)
{
    string result;
    if (size < 0)
        return "";
    for (int i = 0; i < size; i++)
    {
        result += c & 0x01 ? "1" : "0";
        c = c >> 1;
    }
    reverse(result.begin(), result.end());
    return result;
}

string convert_zl78_to_bitArray(vector<pair<string, int>> const &compressed_text)
{
    string res;

    for (int i = 0; i < compressed_text.size(); i++)
    {
        res += value_to_bit(floor(log2(i)) + 1, compressed_text[i].second);
        res += value_to_bit(8, compressed_text[i].first[0]);
    }
    return res;
}

void print_map(vector<pair<string, int>> const &m)
{
    for (auto const &pair : m)
        cout << "(" << pair.second << ", " << pair.first << "),";
    cout << endl;
}

string decompress_zl78(vector<pair<string, int>> const &compressed_text)
{
    string res;
    string tmp_res;
    vector<pair<string, int>> dictionary;
    int dictionnary_index = 1;

    dictionary.push_back(make_pair("", 0));
    for (int i = 0; i < compressed_text.size(); i++)
    {

        if (compressed_text[i].second > 0)
            tmp_res += dictionary[compressed_text[i].second].first;
        tmp_res += compressed_text[i].first;
        dictionary.push_back(make_pair(tmp_res, dictionnary_index++));
        res += tmp_res;
        tmp_res = "";
    }
    return res;
}

int find_in_map(vector<pair<string, int>> const &map, string tofind)
{
    for (auto const &pair : map)
    {
        if (pair.first == tofind)
            return pair.second;
    }
    return -1;
}

vector<pair<string, int>> compress_lz78(string entry)
{
    vector<pair<string, int>> dictionary;
    vector<pair<string, int>> res;

    int dictionnary_index = 1;

    string current_string;
    string tmp_res;

    for (int i = 0; i < entry.size(); i++)
    {
        current_string = current_string + entry[i];
        if (find_in_map(dictionary, current_string) < 0)
        {
            dictionary.push_back(make_pair(current_string, dictionnary_index++));
            tmp_res = current_string.substr(0, current_string.size() - 1);
            if (find_in_map(dictionary, tmp_res) >= 0)
            {
                res.push_back(make_pair(entry.substr(i, 1), find_in_map(dictionary, tmp_res)));
            }
            else
                res.push_back(make_pair(entry.substr(i, 1), 0));
            current_string = "";
        }
    }
    if (current_string.size())
    {
        if (find_in_map(dictionary, current_string) >= 0)
            res.push_back(make_pair("\0", find_in_map(dictionary, current_string)));
        else
            res.push_back(make_pair(current_string, 0));
    }
    return res;
}

vector<pair<string, int>> compress_lz78_j0j0(vector<string> entry)
{
}

int main()
{
    string buff;
    string entry;
    ifstream MyReadFile("text");
    while (getline(MyReadFile, buff))
        entry += buff + "\n";
    cout << entry << endl;
    vector<pair<string, int>> res = compress_lz78(entry);
    cout
        << "\nEND RESULT :" << endl;
    print_map(res);
    cout << convert_zl78_to_bitArray(res) << endl;
    cout << "DECOMPRESS " << decompress_zl78(res) << endl;

    cout << entry.size() << " " << res.size() * 2 << " " << ceil(convert_zl78_to_bitArray(res).size() / 8);
    return 0;
}